// a test bench for decoder_3to8_en module
`timescale 1 ns/100 ps
module decoder_3to8_en_tb;
// internal signals declaration

    reg [2:0]x_in;
    reg en;
    wire [7:0]Y;
    //Unit Under Test instance and port map
    decoder_3to8_en UUT(.x_in(x_in),.en(en),.Y(Y));


    initial begin
           en=1'b0;
       #50 en=1'b1;
    end
    
    integer i;
    initial begin
       for(i=0;i<20;i=i+1)begin
             #50 x_in=i;
       end
    end
   
    initial #1000 $finish;
    initial  //response monitoring block
        $monitor($realtime,"ns %h %h %h",x_in,en,Y);
endmodule

