<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="x_in(2)">
        </signal>
        <signal name="x_in(0)">
        </signal>
        <signal name="XLXN_12" />
        <signal name="XLXN_11" />
        <signal name="x_in(1)">
        </signal>
        <signal name="AA">
        </signal>
        <signal name="Y(5)">
        </signal>
        <signal name="Y(6)">
        </signal>
        <signal name="Y(7)">
        </signal>
        <signal name="XLXN_13" />
        <signal name="en" />
        <signal name="XLXN_85" />
        <signal name="XLXN_86" />
        <signal name="XLXN_87" />
        <signal name="XLXN_88" />
        <signal name="Y(0)">
        </signal>
        <signal name="Y(1)">
        </signal>
        <signal name="Y(2)">
        </signal>
        <signal name="Y(3)">
        </signal>
        <signal name="XLXN_98" />
        <signal name="x_in(2:0)" />
        <signal name="Y(7:0)" />
        <signal name="XLXN_101" />
        <signal name="Y(4)" />
        <signal name="XLXN_103" />
        <signal name="XLXN_104" />
        <signal name="XLXN_105" />
        <signal name="XLXN_106" />
        <signal name="XLXN_107" />
        <signal name="XLXN_108" />
        <signal name="XLXN_109" />
        <port polarity="Input" name="en" />
        <port polarity="Input" name="x_in(2:0)" />
        <port polarity="Output" name="Y(7:0)" />
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <block symbolname="inv" name="XLXI_1">
            <blockpin signalname="x_in(0)" name="I" />
            <blockpin signalname="XLXN_11" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_2">
            <blockpin signalname="x_in(1)" name="I" />
            <blockpin signalname="XLXN_12" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_7">
            <blockpin signalname="XLXN_13" name="I0" />
            <blockpin signalname="XLXN_12" name="I1" />
            <blockpin signalname="XLXN_11" name="I2" />
            <blockpin signalname="XLXN_85" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_17">
            <blockpin signalname="XLXN_13" name="I0" />
            <blockpin signalname="XLXN_12" name="I1" />
            <blockpin signalname="x_in(0)" name="I2" />
            <blockpin signalname="XLXN_86" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_18">
            <blockpin signalname="XLXN_13" name="I0" />
            <blockpin signalname="x_in(1)" name="I1" />
            <blockpin signalname="XLXN_11" name="I2" />
            <blockpin signalname="XLXN_87" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_19">
            <blockpin signalname="XLXN_13" name="I0" />
            <blockpin signalname="x_in(1)" name="I1" />
            <blockpin signalname="x_in(0)" name="I2" />
            <blockpin signalname="XLXN_88" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_20">
            <blockpin signalname="XLXN_11" name="I0" />
            <blockpin signalname="XLXN_12" name="I1" />
            <blockpin signalname="x_in(2)" name="I2" />
            <blockpin signalname="AA" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_28">
            <blockpin signalname="x_in(0)" name="I0" />
            <blockpin signalname="XLXN_12" name="I1" />
            <blockpin signalname="x_in(2)" name="I2" />
            <blockpin signalname="XLXN_108" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_29">
            <blockpin signalname="XLXN_11" name="I0" />
            <blockpin signalname="x_in(1)" name="I1" />
            <blockpin signalname="x_in(2)" name="I2" />
            <blockpin signalname="XLXN_106" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_30">
            <blockpin signalname="x_in(0)" name="I0" />
            <blockpin signalname="x_in(1)" name="I1" />
            <blockpin signalname="x_in(2)" name="I2" />
            <blockpin signalname="XLXN_109" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_3">
            <blockpin signalname="x_in(2)" name="I" />
            <blockpin signalname="XLXN_13" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_31">
            <blockpin signalname="en" name="I0" />
            <blockpin signalname="XLXN_85" name="I1" />
            <blockpin signalname="Y(0)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_44">
            <blockpin signalname="en" name="I0" />
            <blockpin signalname="XLXN_86" name="I1" />
            <blockpin signalname="Y(1)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_45">
            <blockpin signalname="en" name="I0" />
            <blockpin signalname="XLXN_87" name="I1" />
            <blockpin signalname="Y(2)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_46">
            <blockpin signalname="en" name="I0" />
            <blockpin signalname="XLXN_88" name="I1" />
            <blockpin signalname="Y(3)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_47">
            <blockpin signalname="AA" name="I0" />
            <blockpin signalname="en" name="I1" />
            <blockpin signalname="Y(4)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_48">
            <blockpin signalname="XLXN_108" name="I0" />
            <blockpin signalname="en" name="I1" />
            <blockpin signalname="Y(5)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_49">
            <blockpin signalname="XLXN_106" name="I0" />
            <blockpin signalname="en" name="I1" />
            <blockpin signalname="Y(6)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_50">
            <blockpin signalname="XLXN_109" name="I0" />
            <blockpin signalname="en" name="I1" />
            <blockpin signalname="Y(7)" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="x_in(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="389" y="1536" type="branch" />
            <wire x2="389" y1="1536" y2="1536" x1="256" />
            <wire x2="464" y1="1536" y2="1536" x1="389" />
            <wire x2="512" y1="1536" y2="1536" x1="464" />
            <wire x2="464" y1="1536" y2="1648" x1="464" />
            <wire x2="1152" y1="1648" y2="1648" x1="464" />
            <wire x2="1152" y1="1648" y2="1776" x1="1152" />
            <wire x2="1392" y1="1648" y2="1648" x1="1152" />
            <wire x2="1392" y1="1648" y2="1776" x1="1392" />
            <wire x2="1632" y1="1648" y2="1648" x1="1392" />
            <wire x2="1872" y1="1648" y2="1648" x1="1632" />
            <wire x2="1872" y1="1648" y2="1776" x1="1872" />
            <wire x2="1632" y1="1648" y2="1776" x1="1632" />
        </branch>
        <branch name="XLXN_12">
            <wire x2="1088" y1="1424" y2="1424" x1="736" />
            <wire x2="1328" y1="1424" y2="1424" x1="1088" />
            <wire x2="1328" y1="1424" y2="1776" x1="1328" />
            <wire x2="1088" y1="1424" y2="1776" x1="1088" />
            <wire x2="1088" y1="1104" y2="1424" x1="1088" />
            <wire x2="1328" y1="1104" y2="1424" x1="1328" />
        </branch>
        <branch name="XLXN_11">
            <wire x2="1024" y1="1296" y2="1296" x1="736" />
            <wire x2="1504" y1="1296" y2="1296" x1="1024" />
            <wire x2="1504" y1="1296" y2="1776" x1="1504" />
            <wire x2="1024" y1="1296" y2="1776" x1="1024" />
            <wire x2="1024" y1="1104" y2="1296" x1="1024" />
            <wire x2="1504" y1="1104" y2="1296" x1="1504" />
        </branch>
        <branch name="x_in(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="377" y="1376" type="branch" />
            <wire x2="377" y1="1376" y2="1376" x1="256" />
            <wire x2="464" y1="1376" y2="1376" x1="377" />
            <wire x2="1568" y1="1376" y2="1376" x1="464" />
            <wire x2="1808" y1="1376" y2="1376" x1="1568" />
            <wire x2="1808" y1="1376" y2="1776" x1="1808" />
            <wire x2="1568" y1="1376" y2="1776" x1="1568" />
            <wire x2="464" y1="1376" y2="1424" x1="464" />
            <wire x2="512" y1="1424" y2="1424" x1="464" />
            <wire x2="1568" y1="1104" y2="1376" x1="1568" />
            <wire x2="1808" y1="1104" y2="1376" x1="1808" />
        </branch>
        <branch name="AA">
            <wire x2="1088" y1="2032" y2="2112" x1="1088" />
        </branch>
        <instance x="512" y="1328" name="XLXI_1" orien="R0" />
        <instance x="512" y="1456" name="XLXI_2" orien="R0" />
        <instance x="960" y="1776" name="XLXI_20" orien="R90" />
        <instance x="1200" y="1776" name="XLXI_28" orien="R90" />
        <instance x="1680" y="1776" name="XLXI_30" orien="R90" />
        <instance x="512" y="1568" name="XLXI_3" orien="R0" />
        <iomarker fontsize="28" x="368" y="1776" name="en" orien="R180" />
        <branch name="XLXN_85">
            <wire x2="1088" y1="784" y2="848" x1="1088" />
        </branch>
        <branch name="XLXN_86">
            <wire x2="1328" y1="784" y2="848" x1="1328" />
        </branch>
        <branch name="XLXN_87">
            <wire x2="1568" y1="784" y2="848" x1="1568" />
        </branch>
        <branch name="XLXN_88">
            <wire x2="1808" y1="784" y2="848" x1="1808" />
        </branch>
        <branch name="Y(0)">
            <attrtext style="alignment:SOFT-TVCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1120" y="480" type="branch" />
            <wire x2="1120" y1="416" y2="480" x1="1120" />
            <wire x2="1120" y1="480" y2="528" x1="1120" />
        </branch>
        <branch name="Y(1)">
            <attrtext style="alignment:SOFT-TVCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1360" y="448" type="branch" />
            <wire x2="1360" y1="416" y2="448" x1="1360" />
            <wire x2="1360" y1="448" y2="528" x1="1360" />
        </branch>
        <instance x="1216" y="1104" name="XLXI_7" orien="R270" />
        <instance x="1456" y="1104" name="XLXI_17" orien="R270" />
        <instance x="1696" y="1104" name="XLXI_18" orien="R270" />
        <branch name="XLXN_13">
            <wire x2="1152" y1="1536" y2="1536" x1="736" />
            <wire x2="1392" y1="1536" y2="1536" x1="1152" />
            <wire x2="1632" y1="1536" y2="1536" x1="1392" />
            <wire x2="1872" y1="1536" y2="1536" x1="1632" />
            <wire x2="1152" y1="1104" y2="1536" x1="1152" />
            <wire x2="1392" y1="1104" y2="1536" x1="1392" />
            <wire x2="1632" y1="1104" y2="1536" x1="1632" />
            <wire x2="1872" y1="1104" y2="1536" x1="1872" />
        </branch>
        <instance x="1936" y="1104" name="XLXI_19" orien="R270" />
        <instance x="1456" y="784" name="XLXI_44" orien="R270" />
        <instance x="1696" y="784" name="XLXI_45" orien="R270" />
        <instance x="1936" y="784" name="XLXI_46" orien="R270" />
        <instance x="1216" y="784" name="XLXI_31" orien="R270" />
        <branch name="en">
            <wire x2="880" y1="1776" y2="1776" x1="368" />
            <wire x2="880" y1="1776" y2="2080" x1="880" />
            <wire x2="1152" y1="2080" y2="2080" x1="880" />
            <wire x2="1392" y1="2080" y2="2080" x1="1152" />
            <wire x2="1632" y1="2080" y2="2080" x1="1392" />
            <wire x2="1632" y1="2080" y2="2112" x1="1632" />
            <wire x2="1872" y1="2080" y2="2080" x1="1632" />
            <wire x2="1872" y1="2080" y2="2112" x1="1872" />
            <wire x2="1392" y1="2080" y2="2112" x1="1392" />
            <wire x2="1152" y1="2080" y2="2112" x1="1152" />
            <wire x2="1152" y1="800" y2="800" x1="880" />
            <wire x2="1392" y1="800" y2="800" x1="1152" />
            <wire x2="1632" y1="800" y2="800" x1="1392" />
            <wire x2="1872" y1="800" y2="800" x1="1632" />
            <wire x2="880" y1="800" y2="1776" x1="880" />
            <wire x2="1152" y1="784" y2="800" x1="1152" />
            <wire x2="1392" y1="784" y2="800" x1="1392" />
            <wire x2="1632" y1="784" y2="800" x1="1632" />
            <wire x2="1872" y1="784" y2="800" x1="1872" />
        </branch>
        <branch name="x_in(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="367" y="1248" type="branch" />
            <wire x2="367" y1="1248" y2="1248" x1="256" />
            <wire x2="464" y1="1248" y2="1248" x1="367" />
            <wire x2="1264" y1="1248" y2="1248" x1="464" />
            <wire x2="1744" y1="1248" y2="1248" x1="1264" />
            <wire x2="1744" y1="1248" y2="1776" x1="1744" />
            <wire x2="1264" y1="1248" y2="1776" x1="1264" />
            <wire x2="464" y1="1248" y2="1296" x1="464" />
            <wire x2="512" y1="1296" y2="1296" x1="464" />
            <wire x2="1264" y1="1104" y2="1248" x1="1264" />
            <wire x2="1744" y1="1104" y2="1248" x1="1744" />
        </branch>
        <branch name="x_in(2:0)">
            <wire x2="160" y1="1120" y2="1248" x1="160" />
            <wire x2="160" y1="1248" y2="1376" x1="160" />
            <wire x2="160" y1="1376" y2="1536" x1="160" />
            <wire x2="160" y1="1536" y2="1616" x1="160" />
        </branch>
        <iomarker fontsize="28" x="160" y="1120" name="x_in(2:0)" orien="R270" />
        <bustap x2="256" y1="1248" y2="1248" x1="160" />
        <bustap x2="256" y1="1376" y2="1376" x1="160" />
        <bustap x2="256" y1="1536" y2="1536" x1="160" />
        <branch name="Y(7:0)">
            <wire x2="1120" y1="2640" y2="2640" x1="960" />
            <wire x2="1360" y1="2640" y2="2640" x1="1120" />
            <wire x2="1600" y1="2640" y2="2640" x1="1360" />
            <wire x2="1840" y1="2640" y2="2640" x1="1600" />
            <wire x2="2080" y1="2640" y2="2640" x1="1840" />
            <wire x2="1120" y1="320" y2="320" x1="1024" />
            <wire x2="1360" y1="320" y2="320" x1="1120" />
            <wire x2="1600" y1="320" y2="320" x1="1360" />
            <wire x2="1840" y1="320" y2="320" x1="1600" />
            <wire x2="2080" y1="320" y2="320" x1="1840" />
            <wire x2="2080" y1="320" y2="2640" x1="2080" />
        </branch>
        <iomarker fontsize="28" x="960" y="2640" name="Y(7:0)" orien="R180" />
        <bustap x2="1840" y1="2640" y2="2544" x1="1840" />
        <bustap x2="1600" y1="2640" y2="2544" x1="1600" />
        <bustap x2="1360" y1="2640" y2="2544" x1="1360" />
        <bustap x2="1120" y1="2640" y2="2544" x1="1120" />
        <bustap x2="1360" y1="320" y2="416" x1="1360" />
        <bustap x2="1600" y1="320" y2="416" x1="1600" />
        <bustap x2="1840" y1="320" y2="416" x1="1840" />
        <bustap x2="1120" y1="320" y2="416" x1="1120" />
        <branch name="Y(2)">
            <attrtext style="alignment:SOFT-TVCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1600" y="448" type="branch" />
            <wire x2="1600" y1="416" y2="448" x1="1600" />
            <wire x2="1600" y1="448" y2="528" x1="1600" />
        </branch>
        <branch name="Y(3)">
            <attrtext style="alignment:SOFT-TVCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1840" y="448" type="branch" />
            <wire x2="1840" y1="416" y2="448" x1="1840" />
            <wire x2="1840" y1="448" y2="528" x1="1840" />
        </branch>
        <branch name="Y(5)">
            <attrtext style="alignment:SOFT-TVCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1360" y="2480" type="branch" />
            <wire x2="1360" y1="2368" y2="2480" x1="1360" />
            <wire x2="1360" y1="2480" y2="2544" x1="1360" />
        </branch>
        <branch name="Y(6)">
            <attrtext style="alignment:SOFT-TVCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1600" y="2480" type="branch" />
            <wire x2="1600" y1="2368" y2="2480" x1="1600" />
            <wire x2="1600" y1="2480" y2="2544" x1="1600" />
        </branch>
        <branch name="Y(7)">
            <attrtext style="alignment:SOFT-TVCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1840" y="2480" type="branch" />
            <wire x2="1840" y1="2368" y2="2480" x1="1840" />
            <wire x2="1840" y1="2480" y2="2544" x1="1840" />
        </branch>
        <branch name="Y(4)">
            <attrtext style="alignment:SOFT-TVCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1120" y="2480" type="branch" />
            <wire x2="1120" y1="2368" y2="2480" x1="1120" />
            <wire x2="1120" y1="2480" y2="2544" x1="1120" />
        </branch>
        <instance x="1440" y="1776" name="XLXI_29" orien="R90" />
        <instance x="1504" y="2112" name="XLXI_49" orien="R90" />
        <branch name="XLXN_106">
            <wire x2="1568" y1="2032" y2="2112" x1="1568" />
        </branch>
        <instance x="1264" y="2112" name="XLXI_48" orien="R90" />
        <instance x="1024" y="2112" name="XLXI_47" orien="R90" />
        <instance x="1744" y="2112" name="XLXI_50" orien="R90" />
        <branch name="XLXN_108">
            <wire x2="1328" y1="2032" y2="2112" x1="1328" />
        </branch>
        <branch name="XLXN_109">
            <wire x2="1808" y1="2032" y2="2112" x1="1808" />
        </branch>
    </sheet>
</drawing>